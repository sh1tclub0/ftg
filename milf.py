from .. import loader, utils

url = "https://x0.at/ipsU.jpg"


@loader.tds
class BigRatMod(loader.Module):
    """Anime this is not shit."""
    strings = {"name": "Milf"}

    async def milfcmd(self, message):
        """Usage: .milf (user)"""
        target = await self.get_target(message)
        if not target:
            return await message.edit("<b>Please specify who to anime.</b>")

        msg = await utils.answer(message, "<b>Sending big rat...</b>")
        await message.client.send_file(target, url)
        await utils.answer(msg, "<b>Sent anime successfully!</b>")

    @staticmethod
    async def get_target(message):
        if message.chat_id > 0:
            return await message.client.get_entity(message.chat_id)
        args = utils.get_args_raw(message)
        if args:
            args = args.split()[0]
        reply = await message.get_reply_message()

        if not args and not reply:
            return None
        if reply and reply.from_id:
            return reply.from_id

        user = args if not args.isnumeric() else int(args)
        return await message.client.get_entity(user)
